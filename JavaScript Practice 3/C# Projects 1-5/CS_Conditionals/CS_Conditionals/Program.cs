﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            //Temp convert
            Console.WriteLine("Would you like to convert to Fahrenheit (F) or Celsius (C)?");
            string temp = Console.ReadLine();
            if (temp == "Fahrenheit" || temp == "F" || temp == "fahrenheit" || temp == "f")
            {
                Console.WriteLine("Please enter the temp in Celsius to convert to Fahrenheit");
                string temperature = Console.ReadLine();
                int mytemp = int.Parse(temperature);

                Console.WriteLine("Your new temp is: {0}", mytemp);
            }
            else if (temp == "Celsius" || temp == "C" || temp == "celsius" || temp == "c")
            {
                Console.WriteLine("Please enter the temp in Fahrenheit to convert to Celsius");
                string tem1p = Console.ReadLine();
                int tem12p = int.Parse(tem1p);
                int cels = ((tem12p - 32) * 5 / 9);
                Console.WriteLine("Your new temp is: {0}", cels);
            }
            else
            {
                Console.WriteLine("Kari does not compute");
            }


            //Last chance for gas
            Console.WriteLine("Please enter how many gallons your tank can hold");
            string gallonString = Console.ReadLine();
            Console.WriteLine("Please enter how full your tank is in %");
            string gasGaugeString = Console.ReadLine();
            Console.WriteLine("How many miles per gallon does your vehicle go?");
            string mpgString = Console.ReadLine();
            int gallon = int.Parse(gallonString);
            int gasGauge = int.Parse(gasGaugeString);
            int mpg = int.Parse(mpgString);
            int gas = gasGauge / 100;
            int tank = gallon * gas;
            int miles = tank * mpg;
            if (miles >= 200)
            {
                Console.WriteLine("You've got plenty of gas to make it to the next gas station")
            }
            else if (miles <= 199)
            {
                Console.WriteLine("It'd be best to stop for gas now.")
            }
            else
            {
                Console.WriteLine("Kari does not compute");
            }


            //Grade Convert
            int a = 90;
            int b = 80;
            int c = 73;
            int d = 70;
            Console.WriteLine("Please enter the grade you received");
            string userGradeString = Console.ReadLine();
            int userGrade = int.Parse(userGradeString);
            if (userGrade >= a && userGrade <= 100)
            {
                Console.WriteLine("You got an A!");
            }
            else if (userGrade >= b && userGrade < a)
            {
                Console.WriteLine("You got a B!");
            }
            else if (userGrade >= c && userGrade < b)
            {
                Console.WriteLine("You got a C!");
            }
            else if (userGrade >= d && userGrade < c)
            {
                Console.WriteLine("You got a D!");
            }
            else
            {
                Console.WriteLine("You got an F!");
            }


            //Discount Double Check
            Console.WriteLine("Please enter the cost of the first item");
            string firstString = Console.ReadLine();
            Console.WriteLine("Please enter the cost of the second item");
            string secondString = Console.ReadLine();
            double first = double.Parse(firstString);
            double second = double.Parse(secondString);
            double subTotalCost = first + second

            if (subTotalCost >= 100)
            {
                double discount1 = 0.10;
                double discountCost = subTotalCost * discount1;
                double totalCost10 = subTotalCost - discountCost;
                Console.WriteLine("Your total cost with a ten percent discount is: ${0}",totalCost10);
            }
            else if (subTotalCost >= 50 && subTotalCost < 100)
            {
                double discount2 = 0.05;
            
                double discountCost2 = subTotalCost * discount2;
                double totalCost5 = subTotalCost - discountCost2;
                Console.WriteLine("Your total cost with a five percent discount is: ${0}", totalCost5);
            }
            else
            {
                Console.WriteLine("Your total cost is: ${0}", subTotalCost);

            }

        }
    }
}



    /*


//Discount Double Check




 */
 }
