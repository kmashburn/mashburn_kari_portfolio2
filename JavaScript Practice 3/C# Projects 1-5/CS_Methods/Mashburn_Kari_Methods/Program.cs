﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mashburn_Kari_Methods
{
    /*Kari Mashburn
     * 9/14/2016
     * Scalable Data Infrastructures
     * Methods
     
         */
    class Program
    {
        static void Main(string[] args)
        {
            //Circumference
            double circle = CircomfCircle(0);
            Console.WriteLine("The circumference of your lovely circle is: " + circle);

            //Bee Strings
            double stung = beeStings(0);
            Console.WriteLine("\n\rIt takes " + stung + " to kill your victim. MUHAHAHAHA >:)");

            //Problem 3 - Reverse It
            //Defines the array
            string[] gGods = new string[5] { "Zeus", "Hades", "Athena", "Poseidon", "Ares" };
            //For loop to print the array out forward
            Console.WriteLine("\r\nArray in correct order: ");
            for (int i = 0; i < gGods.Length; i++) 
            {
                Console.WriteLine(gGods[i]);
            }
            //For loop to print array out in reverse
            Console.WriteLine("\r\nArray in reverse order: ");
            for (int i = gGods.Length-1; i>=0; i--)
            {
                Console.WriteLine(gGods[i]);
            }

        }


        //Problem 1 - Circumfrence 
        public static double CircomfCircle(double Ccircle)
        {
            Console.WriteLine("Hey there, this is the Circumfrence Calculator");
            Console.WriteLine("Let's start by entering the radius of the circle: ");
            string radiusCircle = Console.ReadLine();
            double radius;

            double.TryParse(radiusCircle, out radius);

            Ccircle = 2 * (double)Math.PI * radius; //Calculates Circumference from 2*pi*r

            return Ccircle;

            /*Test values
             *User entered 3 circumference returned 18.8495559215388
             *User entered 60 circumference returned 376.991118430775
             *Checked by calculator - works :) 
             */
        }

        //Problem 2 - Stung!
        public static double beeStings(double beeStung)
        {
            Console.WriteLine("\r\nI am allergic to bee stings, but let's find out how many stings it takes to kill you! :) ");
            Console.WriteLine("Are you trying to kill someone?? >:)");
            Console.WriteLine("\r\nWhat is the victom's weight? (in lbs): ");
            string weightString = Console.ReadLine();
            double weight;

            double.TryParse(weightString, out weight);

            double beesSuck = 8.666666667 * weight;

            return beesSuck;
            /*Test Values!!
             * User entered a weight of 140 returned 1213.33333338
             * User entered a weight of 185 returned 1603.333333395
             * Checked with a calculator, is correct
             */
        }

    }
}
