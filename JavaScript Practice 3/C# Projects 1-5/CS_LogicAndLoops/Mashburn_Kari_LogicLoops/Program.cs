﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mashburn_Kari_LogicLoops
{
    /*Kari Mashburn
     9/12/2016
     Scalable Data Infrastructures
     Loops and Logic 
     Day 4 Lab*/


    class Program
    {
        static void Main(string[] args)
        {
            //Convert Strings to int
            int frontTires;
            int rearTires;
            //Problem 1: Tire Pressure

            //Asking the user tire pressure for front tires
            Console.WriteLine("What is your current front tire pressure?");
            string frontTiresString = Console.ReadLine();

            while (!(int.TryParse(frontTiresString, out frontTires)))
            {
                Console.WriteLine("Oops, something went wrong, please only use numbers");
                Console.WriteLine("\n\rWhat is your current front tire pressure?");
                frontTiresString = Console.ReadLine();
            }

            //Asking the user tire pressure for rear tires
            Console.WriteLine("What is your current rear tire pressure?");
            string rearTiresString = Console.ReadLine();
            while(!(int.TryParse(rearTiresString, out rearTires)))
            {
                Console.WriteLine("Oops, something went wrong, please only use numbers");
                Console.WriteLine("\n\rWhat is your current front tire pressure?");
                rearTiresString = Console.ReadLine();
            }

            //Set up if else statements
            if (frontTires >= 30 && rearTires >= 27)
                {
                    Console.WriteLine("The tires pass specs!");

                }

                else
                {
                    Console.WriteLine("Get your tires checked out!");
                }

                //User entered 39 for front tires and 15 for rear tires and got get your tires checked
                //User entered 30 for front tires and 27 for rear tires and got  the tires pass specs


            //Problem 2 Movie Ticket Price

            //Declare variables
            int age;
            int movieTime;
            int moviePrice = 12;
            int matinee = 7;

            //Ask the user
            Console.WriteLine("How old are you?");
            string ageString = Console.ReadLine();
            
            while (!(int.TryParse(ageString, out age)))
            {
                Console.WriteLine("Oops, something went wrong.");
                Console.WriteLine("\n\rPlease enter your age.");
                ageString = Console.ReadLine();
            }

            //Ask the user what time they are seeing the movie
            Console.WriteLine("What time are you seeing the movie? Please enter without the AM or PM");
            string movieTimeString = Console.ReadLine();
            while (!(int.TryParse(movieTimeString, out movieTime)))
            {
                Console.WriteLine("Oops, something went wrong");
                Console.WriteLine("\n\rWhat time are you seeing the movie?");
                movieTimeString = Console.ReadLine();
            }

            if (age >= 55 || age <= 10)
            {
                Console.WriteLine("Your ticket price is "+ matinee + " dollars enjoy your movie");
            }
            
            else if (movieTime>= 3 && movieTime<= 5)
            {
                Console.WriteLine("Since it's matinee your ticket price is " +matinee+ " dollars. Enjoy your movie!");
                
            }
            else
            {
                Console.WriteLine("Your movie ticket price is " +moviePrice + " dollars. Enjoy your movie!");
            }
            //User entered 25 age, 6 movie time and got ticket price of $12.00
            //User entered age 60 and movie time of 12 and got ticket price of $7.00
            //User entered age 3 and movie time of 9 and got ticket price of $7.00
            //User entered age 16 and movie time of 4 and got ticket price of $7.00


            //Problem 3 - Add up the odds

            int[] firstArray = new int[11] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Convert.ToString(firstArray);
            Console.WriteLine("Now we are going to add up all the odd numbers");
            int sum1 = 0 + 1 + 3 + 5 + 7 + 9;
            Console.WriteLine("The sum of the odd numbers are " + sum1);


            //Problem 4 - RPG Battle
            //Declare values
            string fighter1 = "Zeus";
            string fighter2 = "Hades";
            int health1 = 100;
            Random damage1 = new Random();
            int damages = damage1.Next(1, 101);
            int fighters;
            Random rounds = new Random();
            int hurtMe = damages - health1;

            Console.WriteLine("Please enter 1 for Zeus or 2 for Hades");
            string fighterString = Console.ReadLine();
            while (!(int.TryParse(fighterString, out fighters)))
            {
                Console.WriteLine("Oops something went wrong");
                Console.WriteLine("Please enter 1 for Zeus or 2 for Hades");
                fighterString = Console.ReadLine();
            }
            
         
            if (fighters == 1)
            {
                Console.WriteLine("You have chosen " + fighter1 + "\n\rYour competitor is "+ fighter2);
                Console.WriteLine("The health you currently have is "+health1);
                
            }
            else
            {
                Console.WriteLine("You have chosen " + fighter2 + "\n\rYour competitor is " + fighter1);
                Console.WriteLine("The health you currently have is " + health1);
            }


            while (fighters == 1 || fighters == 2)
            {
                if (fighters == 1 && hurtMe <= 0)
                {
                    Console.WriteLine("\n\rYour fighter's health is now " + hurtMe + " and " + fighter2 + " has won");
                    Console.WriteLine("You have died, the battle is lost");
                }
                else if (fighters == 2 && hurtMe >= 0)
                {
                    Console.WriteLine("Your fighter " + fighter2 + " has gone " + rounds + " with " + fighter1);
                    Console.WriteLine("\n\rYour fighter's health is now " + hurtMe + " and " + fighter1 + " has won");
                    Console.WriteLine("You have died, the battle is lost");
                }
                else if (fighters == 1 || fighters == 2 && hurtMe <= 0)
                {
                    Console.WriteLine("You have both died in the war.. There will now be war between the Greek Gods.");
                }
                break;
            }

            }
            




        }
    }

