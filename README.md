# README #

This repository is for Project and Portfolio 2: Web Design and Development

### What is this repository for? ###

* This repository is for my final project, called Do What?
* Do What? is a trivia game that will allow users to choose from 5 different categories, music, movies, games, web applications, and mobile applications, user will be prompted 5 questions per category. 

### What's currently in my repository ###
* My project proposal and flowchart for the final project
* My Database Review and screenshots C# program.
* JSON Practice
* OOP Practice
* Do What?

### Who do I talk to? ###

* Kari Mashburn - kmashburn@fullsail.edu