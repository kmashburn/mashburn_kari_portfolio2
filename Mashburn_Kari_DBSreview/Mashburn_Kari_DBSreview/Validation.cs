﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mashburn_Kari_DBSreview
{
    class Validation
    {
        public static int ValidateInt(string prompt)
        {
            Console.Write(prompt);
            string input = Console.ReadLine();
            int retValue;

            while (Int32.TryParse(input, out retValue) == false)
            {
                Console.Write("\r\n" + prompt);
                input = Console.ReadLine();
            }

            return retValue;
        }

        public static string ValidateString(string prompt)
        {
            Console.Write(prompt);
            string input = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(input))
            {
                Console.Write("\r\n" + prompt);
                input = Console.ReadLine();
            }

            return input;
        }
    }
}
