﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Mashburn_Kari_DBSreview;

namespace Mashburn_Kari_DBSReview { 

    class Program
    {
        // connection string variable
        static MySqlConnection conn = new MySqlConnection();

        static void Main(string[] args)
        {
            string connectionString = "server=192.168.1.66;uid=kari;pwd=pass;database=SampleAPIData;port=8889";

            string city = Validation.ValidateString("Please enter a city: ");

            try
            {
                conn.ConnectionString = connectionString;
                MySqlDataReader read = null;
                conn.Open();

                string sql = $"SELECT city, temp, pressure, humidity FROM weather WHERE city LIKE '%{city}%' LIMIT 1;";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    string print = $"\r\nCity: {read.GetString(0)}\r\nTemp: {read.GetFloat(1)}\r\nPressure: {read.GetFloat(2)}\r\nHumidity: {read.GetInt32(3)}";

                    Console.WriteLine(print);
                }

            }
            catch (MySqlException)
            {
                Console.WriteLine("No Data Available for the selected city");

            }

            Console.ReadLine();
        }

        public static void RetrieveData(string city)
        {
            // create a SQL statement to pull the data
            string sql = $"SELECT city, temp, pressure, humidity FROM weather WHERE city LIKE '{city}' LIMIT 1;";

            MySqlDataReader read = null;

            MySqlCommand cmd = new MySqlCommand(sql, conn);

            read = cmd.ExecuteReader();


        }

    }
}
