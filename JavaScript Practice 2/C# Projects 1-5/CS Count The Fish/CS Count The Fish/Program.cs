﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_Count_The_Fish
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] fishArray = { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" };
            Console.WriteLine("Please choose (1) for Red, (2) for Blue, (3) for Green, and (4) for Yellow.");
            string userChoose = Console.ReadLine();
            int userChoice = int.Parse(userChoose);
            string[] fishArrayColor = { "red", "blue", "green", "yellow" };
            int numberOfFish = 0;
            for (int i = 0; i < fishArray.Length; i++)
            {
                var arrayLoop = fishArray[i];

                if (userChoice == 1 && arrayLoop == "red")
                {
                    numberOfFish++;
                }
                else if (userChoice == 2 && arrayLoop == "blue")
                {
                    numberOfFish++;
                }
                else if (userChoice == 3 && arrayLoop == "green")
                {
                    numberOfFish++;
                }
                else if (userChoice == 4 && arrayLoop == "yellow")
                {
                    numberOfFish++;
                }
            }
            if (userChoice == 1)
            {
                Console.WriteLine("There are " + numberOfFish + " fish of the color " + fishArrayColor[0]);
            }
            else if (userChoice == 2)
            {
                Console.WriteLine("There are " + numberOfFish + " of the color " + fishArrayColor[1]);
            }
            else if (userChoice == 3)
            {
                Console.WriteLine("There are " + numberOfFish + " fish of the color " + fishArrayColor[2]);
            }
            else if (userChoice == 4)
            {
                Console.WriteLine("There are " + numberOfFish + " fish of the color " + fishArrayColor[3]);
            }

        }
    }
}
