﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mashburn_Kari_StringObjects2
{
    /*Kari Mashburn
     * 9/21/2016
     * SDI
     * String Objects
     */
    class Program
    {
        static void Main(string[] args)
        {
            //Problem 1 Email Address Checker
            Console.WriteLine("Hi Dan, welcome to problem 1, the email address checker");
            Console.WriteLine("Please enter your email address to test");
            string emailAddress = Console.ReadLine();
            Console.WriteLine("The email you provided is: " + emailAddress);
            string tester = problem1(emailAddress);
            Console.WriteLine(tester);

            //Problem 2 Seperator Swap Out
            Console.WriteLine("\n\rThis is problem 2, Sperator Swap Out");
            string list = "z,y,w,x";
            char commaSeperator = ',';
            char slashSperator = '/';
            string newList1 = problem2(list,commaSeperator,slashSperator);
            Console.WriteLine("The first list of items is {0}", list);
            Console.WriteLine("The new list of items swapped is {0}", newList1);
        }
        public static string problem1(string emailTester)
        {
            //Strings to return to main method
            string emailSuccess = "Yay, you've entered a valid email address Dan!";
            string emailFailure = "Oh no Dan, you entered an email address that isn't valid!";

            //Declare variables to test 
            int atSymbol1 = emailTester.IndexOf("@");
            int atSymbol2 = emailTester.LastIndexOf("@");
            int periodTest2 = emailTester.LastIndexOf(".");
            int periodTest1 = emailTester.IndexOf(".");
            //Conditional to check if variables match for atSymbol and spaces
            if (atSymbol1 == atSymbol2 && emailTester.Contains(" ") == false && periodTest1==periodTest2)
            {
                {
                    return emailSuccess;
                }
            }
            else
            {
                return emailFailure;
            }
        }
        public static string problem2(string list, char comma, char slash)
        {
            //This swaps the commas out to the slashes
            string newList = list.Replace(comma , slash);
            //Returns new value
            return newList;
        }
    }
}
