﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mashburn_Kari_ArrayLists
{ /*Kari Mashburn
    9/15/2016
    SDI
    Array Lists
    */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\r\n");

            //Creating and Calling ArrayList's
            NascarList1();
            Console.WriteLine("\r\n");

            

            
        }
        public static void NascarList1()
        {
            //NASCAR Drivers
            ArrayList nascarDrivers = new ArrayList() {"Chase Elliot"};
            nascarDrivers.Add("Kyle Busch");
            nascarDrivers.Add("Dale Earnhardt Jr.");
            nascarDrivers.Add("Jeff Gordon");
            nascarDrivers.Add("Martin Truex Jr.");
            nascarDrivers.Add("Dale Earnhardt");
            nascarDrivers.Add("Austin Dillon");
            nascarDrivers.Add("Jimmie Johnson");
            nascarDrivers.Add("Matt Kenseth");
            nascarDrivers.Add("Carl Edwards");
            nascarDrivers.Add("Denny Hamlin");

            //Removing some of the drivers because they are either out for the season
            //Or they are retired or passed away
            //Dale Jr out for the 2016 season with concussion like symptoms 
            nascarDrivers.Remove("Dale Earnhardt Jr.");
            //Jeff Gordon has retired as of 2015
            nascarDrivers.Remove("Jeff Gordon");
            //Dale Earnhardt passed away in Feb. 2001
            nascarDrivers.Remove("Dale Earnhardt");

            foreach(string nascarDriver in nascarDrivers)
            {
                Console.WriteLine("{0} is a NASCAR driver" , nascarDriver);
            }
            Console.WriteLine("\r\n");


            ArrayList nascarSponsors = new ArrayList() { "Napa Auto Parts" };
            nascarSponsors.Add("M&Ms");
            nascarSponsors.Add("Nationwide Insurance");
            nascarSponsors.Add("Axalta");
            nascarSponsors.Add("Furniture Row");
            nascarSponsors.Add("Goodwrench Service Plus");
            nascarSponsors.Add("DOW Packaging");
            nascarSponsors.Add("Lowes Home Improvment");
            nascarSponsors.Add("DeWalt FlexBolt");
            nascarSponsors.Add("Stanley");
            nascarSponsors.Add("FedEx Express");

            //Removing the sponsors from the list that corrisponds to the drivers on the above list
            nascarSponsors.Remove("Nationwide Insurance");
            nascarSponsors.Remove("Axalta");
            nascarSponsors.Remove("Goodwrench Service Plus");

            foreach (string sponsor in nascarSponsors)
            {
                Console.WriteLine("{0} are sponsors in NASCAR", sponsor);
            }
            Console.WriteLine("\r\n");


            if (nascarDrivers.Count == nascarSponsors.Count)
            {
                for (int i = 0; i < nascarDrivers.Count; i++)
                    Console.WriteLine("{0} is a NASCAR driver and their {1} is their main sponsor", nascarDrivers[i], nascarSponsors[i]);
            }
        }

    }
}
