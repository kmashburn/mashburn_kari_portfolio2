//Logic and Loops Assignment


//Tire Pressure
var rightTirePressure = prompt("What is the tire pressure of the front right tire?");
var leftTirePressure = prompt("What is the tire pressure of the front left tire?");
var backRightPressure = prompt("What is the tire pressure of the back right tire?");
var backLeftPressure = prompt("What is the tire pressure of the back left tire?");
var rightFront = parseInt(rightTirePressure);
var leftFront = parseInt(leftTirePressure);
var rightBack = parseInt(backRightPressure);
var leftBack = parseInt(backLeftPressure);
var tirePressures = [rightFront, leftFront, rightBack, leftBack];

if((tirePressures[0] == tirePressures[1]) && (tirePressures[2] == tirePressures[4]))
{
	console.log("Tires pass spec!");
}
else
{
	console.log("Get your tires checked!");
}

//Movie Ticket Price
var ageString = prompt("Please enter your age");
var age = parseInt(ageString);
var movieTimeString = prompt("What time is the movie? Please enter in 24 hour format")
var movieTime = parseInt(movieTimeString);
var ticketPrice = [7.00, 12.00];
if((age>=55 || age <10)|| (movieTime>=14 && movieTime<=17)) 
{
	console.log("The ticket price is: " + ticketPrice[0]);
}
else
{
	console.log("The ticket price is: " + ticketPrice[1]);
}

//Add up Evens or Odds
var askUser = prompt("Would you like to see the sum of the evens or the odds?");
var theArray = [10, 17, 2, 9, 20, 25];
var baseEven = 0;
var baseOdd = 0;
for(var i=0; i<theArray.length; i++)
{
	var arrayLoop = theArray[i];
	if(arrayLoop % 2 == 0)
	{
		baseEven = baseEven + arrayLoop;
	}
	else if(arrayLoop % 2 != 0)
	{
		baseOdd = baseOdd + arrayLoop;
	}
}
if (askUser == "even" || "Even" || "e")
{
	console.log("The sum of the even numbers is " + baseEven);
}
if(askUser == "odd" || "Odd" || "o")
{
	console.log("The sum of the odd numbers is " + baseOdd);
}
else
{
	console.log("Kari does not compute");
}

//Charge it
var userPrompted = prompt("What is the max credit limit you have?")
var creditLimit = parseInt(userPrompted);
var creditYes = true;
var purchaseAmount;
var purchaseString = prompt("How much is your purchase?");
var purchase = parseInt(purchaseString);
if(creditLimit > 0)
{
	var myCredit = creditLimit -= purchaseAmount;
	var credit = parseInt(myCredit);
	console.log("With your current purchase, you can still spend $" + credit);
}
if(creditLimit <= 0)
{
	var myCredit = creditLimit -= purchaseAmount;
	var credit = parseInt(myCredit);
	console.log("With your last purchase, you have reached your maximum credit limit by $" + credit);
}

