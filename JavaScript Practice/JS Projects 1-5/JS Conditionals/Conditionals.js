//Kari Mashburn
//7/9/2017
//Conditionals

//Temp Convert
var temp = prompt("Would you like to convert to Fahrenheit (F) or Celsius (C)");
if(temp == "Fahrenheit" || temp == "F" || temp == "fahrenheit" || temp == "f")
{
	var newTemp = prompt("Please enter the temp in Celsius to convert to Fahrenheit");
	var newTemp2 = parseInt(newTemp);
	var fahr = ((newTemp2 * 9/5) + 32);
	console.log("Your new temp is: ");
	console.log(fahr);
}
else if(temp == "Celsius" || temp == "C" || temp == "celsius" || temp == "c")
{
	var newTemper = prompt("Please enter the temp in Fahrenheit to convert to Celsius");
	var newTemper2 = parseInt(newTemper);
	var cels = ((newTemper2 - 32) * 5/9);
	console.log("Your new temp is: ");
	console.log(cels);
}
else
{
	window.alert("Kari does not compute");
}


//Last chance for gas
var gallonString = prompt("Please enter how many gallons your tank can hold");
var gasGaugeString = prompt("Please enter how full your tank is in %");
var mpgString = prompt("How many miles per gallon does your vehicle go?");
var gallon = parseInt(gallonString);
var gasGauge = parseInt(gasGaugeString);
var mpg = parseInt(mpgString);
var gas = gasGauge/100;
var tank = gallon * gas;
var miles = tank * mpg;
if(miles>=200)
{
	console.log("You've got plenty of gas to make it to the next gas station")
}
else if (miles<=199)
{
	console.log("It'd be best to stop for gas now.")
}
else
{
	window.alert("Kari does not compute");
}


//Grade Convert
var a = 90;
var b = 80;
var c = 73;
var d = 70;
var f = 69;
var userGrade = prompt("Please enter the grade you received");
if(userGrade >= a && userGrade <= 100)
{
	console.log("You got an A!");
}
else if(userGrade >=b && userGrade < a)
{
	console.log("You got a B!");
}
else if(userGrade>=c && userGrade < b)
{
	console.log("You got a C!");
}
else if(userGrade >= d && userGrade <c)
{
	console.log("You got a D!");
}
else 
{
	console.log("You got an F!");
}


//Discount Double Check
var firstItem = prompt("Please enter the cost of first item");
var secondItem = prompt("Please enter the cost of the second item");
var sub = parseInt(firstItem);
var sub2 = parseInt(secondItem);

if(subTotalCost>= 100)
{
	var subTotalCost = sub + sub2;
	var discount1 = 0.10
	var discountCost = subTotalCost * discount1;
	var totalCost10 = subTotalCost - discountCost;
	console.log("Your total cost with a ten percent discount is:");
	console.log(totalCost10);
}
else if(subTotalCost>=50 && subTotalCost<100)
{
	var subTotalCost = sub + sub2;
	var discount2 = 0.05
	var discountCost2 = subTotalCost * discount2;
	var totalCost5 = subTotalCost - discountCost2;
	console.log("Your total cost with a five percent discount is:");
	console.log(totalCost5);
}
else 
{
	var subTotalCost = sub + sub2;
	console.log("Your total cost is:");
	console.log(subTotalCost)
}



